package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence <T> {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

    private LinkedList<T> tempSubseq = new LinkedList<>() ;

    public boolean find(List<T> x, List<T> y) {
        // TODO: Implement the logic here

        if(x.size() > y.size())
            return false;

        createNewTempSubSeq(x, y);
        if(x.equals(tempSubseq))
            return true;
        else
            return false;
    }

    private void createNewTempSubSeq(List<T> firstSubseq, List<T> secondSubSeq){


        for(int i = 0; i < secondSubSeq.size(); i++){
            for(int j = 0; j < firstSubseq.size(); j++){
                if(secondSubSeq.get(i) == firstSubseq.get(j)){
                    tempSubseq.add(secondSubSeq.get(i));
                }
            }
        }


    }


}
