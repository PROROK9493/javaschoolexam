package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private boolean errorFlag = false;


    public String evaluate(String statement) {
        // TODO: Implement the logic here
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.FLOOR);
        StoreClass sc = plusMinus(statement);
        if(errorFlag){
            return null;
        }
        else {
            Double d = sc.acc;
            d = d *10000;
            int i = (int)Math.round(d);
            d = (double)i/10000;
            return Double.toString(d);
        }



    }

    public StoreClass bracket(String str){
        char first = str.charAt(0);
        if(first == '('){
            StoreClass sc = plusMinus(str.substring(1));
            if(!sc.rect.isEmpty() && sc.rect.charAt(0) == ')'){
                sc.rect = sc.rect.substring(1);
            }
            else{
                errorFlag = true;
                System.err.println("Error: not close bracket");
                return null;
            }
            return sc;
        }
        return numParse(str);
    }

    public StoreClass mulDiv(String str){
        StoreClass sc = bracket(str);
        double acc = sc.acc;

        while (true){
            if(sc.rect.length() == 0){
                return sc;
            }
            char sign = sc.rect.charAt(0);
            if(sign != '*' && sign != '/'){
                return sc;
            }

            String next = sc.rect.substring(1);
            sc = numParse(next);
            if(sign == '*'){
                acc *= sc.acc;
            }
            else{
                acc /= sc.acc;
            }
            sc = new StoreClass(acc, sc.rect);
        }
    }

    public StoreClass plusMinus(String str){
        StoreClass sc = mulDiv(str);
        double acc = sc.acc;

        while (sc.rect.length() > 0){

            if(!(sc.rect.charAt(0) == '+' || sc.rect.charAt(0) == '-'))
                break;

            char sign = sc.rect.charAt(0);
            acc = sc.acc;
            String next = sc.rect.substring(1);
            sc = mulDiv(next);
            if(sign == '+'){
                acc += sc.acc;
            }
            else {
                acc -= sc.acc;
            }
            sc.acc = acc;
        }
        //System.out.println(sc.acc);
        return new StoreClass(sc.acc,sc.rect);
    }

    public StoreClass numParse(String str){
        int dotCnt = 0;
        int i = 0;
        boolean negative = false;

        if(str.charAt(0) == '-'){
            negative = true;
            str = str.substring(1);
        }

        while(i < str.length() && (Character.isDigit(str.charAt(i)) || str.charAt(i) == '.')){
            if(str.charAt(i) == '.' && ++dotCnt > 1){
                errorFlag = true;
                return null;
            }
            i++;
        }

        double rez = Double.parseDouble(str.substring(0,i));
        if (negative){
            rez = -rez;
        }

        return new StoreClass(rez, str.substring(i));
    }

}
