package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private int rows = 1;
    private int colums = 1;

    private int pyramidArray[][];

    private boolean checkBuildPyramid(List<Integer> array){
        int cnt = 0;

        while (cnt < array.size()){
            cnt = (rows * (rows + 1))/2;
            if(cnt >= array.size())
                break;
            rows++;
            colums = colums + 2;
        }

        if(cnt == array.size())
            return true;
        else
            return false;
    }


    public int[][] buildPyramid(List<Integer> inputNumbers)  {
        boolean flagForBuildPyramid = checkBuildPyramid(inputNumbers);

        if(flagForBuildPyramid){
            pyramidArray = new int[rows][colums];
            fillPyramid(inputNumbers);
        }
        else {
            System.out.println("CannotBuildPyramidException");
        }

        return pyramidArray;
    }

    private void fillPyramid(List<Integer> inputArray){

        int middle = colums / 2;
        int cnt = 1;
        int arrIdx = 0;
        List<Integer> sortedInputArray = inputArray.stream().sorted().collect(Collectors.toList());
        fillPyramidByZero();

        for(int i = 0, offset = 0; i < rows; i++, cnt++, offset++){
            int start = middle - offset;
            for(int j = 0; j < cnt*2; j+=2, arrIdx++){
                pyramidArray[i][start + j] = sortedInputArray.get(arrIdx);
            }
        }

    }

    private void fillPyramidByZero(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < colums; j++){
                pyramidArray[i][j] = 0;
            }
        }
    }

    private void printPyramid(){
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < colums; j++){
                System.out.print(pyramidArray[i][j] + " ");
            }
            System.out.println();
        }
    }


    public class CannotBuildPyramidException extends Throwable {
    }


}
